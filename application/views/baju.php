<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<title>Baju Distro</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">
</head>
<body>

<div id="baju">

		<div class="flex">
			<!-- End of Menu Navbar Left -->
			<div class="menu">
				<div class="profile">
					<div class="img-profile" style="background-image: url('<?=base_url()?>assets/img/profile.jpg');"></div>

					<h1><?php echo "Asto Arianto Miwanda"; ?></h1>

					<h2><?php echo "Admin"; ?></h2>
					<div class="line"></div>
				</div>

				<div class="navbar-left">
					<a href="<?=base_url()?>Baju"><div class="navbar-left-list">
						<img src="<?=base_url()?>assets/img/baju.svg">
						<p>Baju Distro</p>
					</div></a>

					<a href="<?=base_url()?>Celana"><div class="navbar-left-list">
						<img src="<?=base_url()?>assets/img/celana.svg">
						<p>Celana Distro</p>
					</div></a>

					<a href="<?=base_url()?>Sepatu"><div class="navbar-left-list">
						<img src="<?=base_url()?>assets/img/sepatu.svg">
						<p>Sepatu Distro</p>
					</div></a>

					<a href="<?=base_url()?>Tas"><div class="navbar-left-list">
						<img src="<?=base_url()?>assets/img/tas.svg">
						<p>Tas Distro</p>
					</div></a>
				</div>
			</div>
			<!-- End of Menu Navbar Left -->

			<!-- Content Right -->
			<div class="content">
				<div class="navbar-top">
					<h3>Distro Me Up</h3>
				</div>

				<div class="content-title">
					<h4><?php echo "Baju"." Distro Me Up"; ?></h4>
					<div class="line"></div>
				</div>

				<!-- Nabvar Content -->
				<form method="get" action="" novalidate="novalidate" class="navbar-content">
					<div class="flex">
						<p>Ukuran</p>
						<select name="ukuran">
							<option value="s">S</option>
							<option value="m">M</option>
							<option value="xl">XL</option>
							<option value="xxl">XXL</option>
						</select>
					</div>

					<div class="flex">
						<p>Tampilkan</p>
						<select name="tampilkan">
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="50">50</option>
						</select>
					</div>

					<div class="flex">
						<input type="text" name="cari" class="input-cari" placeholder="Search">
						<img src="<?=base_url()?>assets/img/search.svg">
					</div>

					<button type="submit" class="btn-submit">Tambah</button>
				</form>
				<!-- End of Nabvar Content -->

				<div class="table-conten">
					<div class="navbar-table-content">
						<h4>Data Baju</h4>
					</div>

					<table>
						<th>
							<td>No</td>
							<td>Merk Baju</td>
							<td>Warna</td>
							<td>Size</td>
							<td>Stock</td>
							<td colspan="2">Aksi</td>
						</th>

						<tr>
							<td>1</td>
							<td>Baju Distro Me Up</td>
							<td>Hitam</td>
							<td>XL</td>
							<td>46</td>
							<td><a href="<?=base_url()?>assets/Baju/Edit">Edit</a></td>
							<td><a href="<?=base_url()?>assets/Baju/Delete">Delete</a></td>
						</tr>
					</table>
				</div>
			</div>
			<!-- End of Content Right -->

		</div>
	
</div>

</body>
</html>